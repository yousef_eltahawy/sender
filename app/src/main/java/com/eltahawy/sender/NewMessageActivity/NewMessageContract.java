package com.eltahawy.sender.NewMessageActivity;

import android.content.Context;

import com.eltahawy.sender.data.Contact.Contact;
import com.eltahawy.sender.data.RoomDB.Message;

import java.util.ArrayList;
import java.util.List;

public interface NewMessageContract {

    interface View{
        void getPhoneNumbers(List<Contact>contacts);
        void sendMessage(List<Contact> contacts,ArrayList<String> Contacts);
    }
    interface Presenter{
        void setPhoneNumbers(String id);
        void sendMessage(List<Contact> contacts);
        void setContext(Context context);
        void setPhoneNums( ArrayList<String> contacts);
    }
}
