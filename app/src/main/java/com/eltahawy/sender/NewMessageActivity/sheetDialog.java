package com.eltahawy.sender.NewMessageActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import com.eltahawy.sender.R;
import com.eltahawy.sender.data.RoomDB.Sheet;
import static com.eltahawy.sender.NewMessageActivity.NewMessageActivity.sheetViewModel;

public class sheetDialog extends AppCompatDialogFragment {
    private String id;
    private String name;
    private EditText editTextId;
    private EditText editTextName;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.sheet_dialog,null);
        editTextId=view.findViewById(R.id.sheetDialogId);
        editTextName=view.findViewById(R.id.sheetDialognemae);
            builder.setView(view)
                    .setTitle("Add New Sheet")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(validate(editTextName))
                                if(validate(editTextId))
                                {
                                    id=editTextId.getText().toString().trim();
                                    name=editTextName.getText().toString().trim();
                                    Sheet sheet=new Sheet();
                                    sheet.setSheetId(id);
                                    sheet.setSheetName(name);
                                    sheetViewModel.insertSheet(sheet);
                                }
                        }
                    });
            return builder.create();
    }
    public boolean validate(EditText ET) {
        String Str = ET.getText().toString().trim();
        if (Str.isEmpty()) {
            System.out.println("dialog 1");
            ET.requestFocus();
            ET.setError("Field can't be empty");
            return false;
        } else {
            System.out.println("dialog 2");
            ET.setError(null);
            return true;
        }
    }
}
