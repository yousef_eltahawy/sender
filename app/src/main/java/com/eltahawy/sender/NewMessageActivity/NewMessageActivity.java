  package com.eltahawy.sender.NewMessageActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.eltahawy.sender.Converters.CsvConverterFactory;
import com.eltahawy.sender.MainActivity.simDialog;
import com.eltahawy.sender.R;
import com.eltahawy.sender.data.Contact.Contact;
import com.eltahawy.sender.data.Contact.ContactApi;
import com.eltahawy.sender.data.RoomDB.Message;
import com.eltahawy.sender.data.RoomDB.Sheet;
import com.eltahawy.sender.data.RoomDB.ViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


 public class NewMessageActivity extends AppCompatActivity implements NewMessageContract.View {
     public static NotificationManagerCompat notificationManager;
     private ArrayList<String> phoneNums = new ArrayList<>();
     private ArrayList<String> contactNames = new ArrayList<>();
     private List<Contact> Contacts = new ArrayList<>();
     private ListView listView;
     private ArrayAdapter adapter;
     private NewMessageContract.Presenter mPresenter;
     public static ViewModel sheetViewModel;
     private List<String> sheetNames = new ArrayList<>();
     private List<String> sheetIds = new ArrayList<>();
     private List<Sheet> sheets = new ArrayList<>();
     private boolean flag;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_new_message);
         flag=true;
         listView = findViewById(R.id.num_list);
         mPresenter = new NewMessagePresenter(this);
         sheetViewModel = new ViewModelProvider(this).get(ViewModel.class);
         sheetViewModel.getAllSheets().observe(this, new Observer<List<Sheet>>() {
             @Override
             public void onChanged(List<Sheet> sheets) {
                 getSheets(sheets);
                 adapter = new ArrayAdapter(getApplicationContext(), R.layout.custom_list_item, sheetNames);
                 listView.setAdapter(adapter);
             }
         });

         listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 if(flag==true) {
                     mPresenter.setPhoneNumbers(sheetIds.get(position));
                 }
             }
         });
         listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
             @Override
             public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                 if(flag==true){
                 PopupMenu popup = new PopupMenu(NewMessageActivity.this, view);
                 MenuInflater inflater = popup.getMenuInflater();
                 inflater.inflate(R.menu.popup_menu, popup.getMenu());
                 popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                     @Override
                     public boolean onMenuItemClick(MenuItem item) {
                         sheetViewModel.deleteSheet(sheets.get(position));
                         return true;
                     }
                 });
                 popup.show();
                 return true;
                 }
                 return false;
             }
         });
     }

     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.contact_item, menu);
         return true;

     }

     @Override
     public boolean onOptionsItemSelected(@NonNull MenuItem item) {
         switch (item.getItemId()) {
             case (R.id.sheets):
                 openDialog();
                 break;
             case (R.id.send):
                 if (phoneNums.isEmpty())
                     Toast.makeText(getApplicationContext(), "there is no contacts", Toast.LENGTH_LONG).show();
                 else
                     sendMessage(Contacts,phoneNums);
                 break;
         }
         return true;
     }

     @Override
     public void getPhoneNumbers(List<Contact> contacts) {
         phoneNums.clear();
         contactNames.clear();
         Contacts.clear();
         try {
         for (Contact contact : contacts)
             if (contact.getStatus().equals("send")) {
                 phoneNums.add("+20"+contact.getPhoneNum());
                 contactNames.add(contact.getName());
                 Contacts.add(contact);
             }
         flag=false;
         adapter = new ArrayAdapter(this, R.layout.custom_list_item, contactNames);
         listView.setAdapter(adapter);
         }
         catch (Exception e){
             Toast.makeText(this,"Check Sheet settings",Toast.LENGTH_LONG).show();
         }
     }

     @Override
     public void sendMessage(List<Contact> contacts,ArrayList<String> phoneNumbers) {
         mPresenter.setContext(getApplicationContext());
         mPresenter.setPhoneNums(phoneNumbers);
         mPresenter.sendMessage(contacts);
         finish();

     }

     public void openDialog() {
         sheetDialog dialog = new sheetDialog();
         dialog.show(getSupportFragmentManager(), "sheet Dialog");
         this.recreate();
     }

     private void getSheets(List<Sheet> Sheets) {
         sheetNames.clear();
         sheetIds.clear();
         sheets.clear();
         sheets=Sheets;
         for (Sheet sheet : Sheets) {
             sheetNames.add(sheet.getSheetName());
             sheetIds.add(sheet.getSheetId().trim());
         }
     }
 }
