package com.eltahawy.sender.NewMessageActivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.SmsManager;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.eltahawy.sender.Converters.CsvConverterFactory;
import com.eltahawy.sender.R;
import com.eltahawy.sender.data.Contact.Contact;
import com.eltahawy.sender.data.Contact.ContactApi;
import com.eltahawy.sender.data.RoomDB.Message;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import static com.eltahawy.sender.MainActivity.MainActivity.messageViewModel;
import static com.eltahawy.sender.MainActivity.MainActivity.simInfo;
import static com.eltahawy.sender.NewMessageActivity.NewMessageActivity.notificationManager;
import static com.eltahawy.sender.Notification.ChannelCreator.CHANNEL_ID;

public class NewMessagePresenter implements NewMessageContract.Presenter {
    private SmsManager smsManager;
    private int SMS_CONTENT_LENGTH_LIMIT = 70;
    private ContactApi contactApi;
    private NewMessageContract.View mView;
    private String SENT = "SMS_SENT";
    private PendingIntent sentPI;
    private ArrayList<PendingIntent> sentPIList;
    private Context context;
    private ArrayList<String> phoneNums=new ArrayList<>();
    private List<Message> messages=new ArrayList<>();

    public NewMessagePresenter(NewMessageContract.View view) {
        mView = view;
        if(simInfo!=null&&Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1){
            smsManager=SmsManager.getSmsManagerForSubscriptionId(simInfo.getSubscriptionId());
        }
        else{
             smsManager = SmsManager.getDefault();
        }
    }
    @Override
    public void setContext(Context context){
        this.context=context;
    }
    @Override
    public void setPhoneNums( ArrayList<String> contacts){
        this.phoneNums=contacts;
    }

    @Override
    public void setPhoneNumbers(String id) {
        CreateRequest(id);
    }

    @Override
    public void sendMessage( List<Contact> contacts) {
        sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        sentPIList=new ArrayList<>();
        notificationManager= NotificationManagerCompat.from(context);
        final int progressMax = phoneNums.size();
        final NotificationCompat.Builder notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_message_24)
                .setContentTitle("send")
                .setContentText("sending in progress")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setProgress(progressMax, 0, false);
        notificationManager.notify(2, notification.build());
        new Thread(new Runnable() {
            @Override
            public void run() {

                int progress=0;
                SystemClock.sleep(2000);
                for (String phoneNum:phoneNums) {
                    String messageContent= contacts.get(progress).getGreeting()+" "+contacts.get(progress).getName()+","
                            +contacts.get(progress).getMessage()+"\nOffer code : "+contacts.get(progress).getOffer_Code()+" "
                            +contacts.get(progress).getSpare()+"\n"+contacts.get(progress).getPage_Link();

                    ArrayList<String> divideMessage = smsManager.divideMessage(messageContent);
                    for(String str:divideMessage){
                        sentPIList.add(sentPI);
                    }
                   if (messageContent.length() <= SMS_CONTENT_LENGTH_LIMIT)
                        smsManager.sendTextMessage(phoneNum, null, messageContent, sentPI, null);
                    else{

                        smsManager.sendMultipartTextMessage(phoneNum, null, divideMessage,sentPIList, null);
                    }
                    Message message=new Message();
                    message.setContactName(contacts.get(progress).getName());
                    message.setMessageContent(messageContent);
                    message.setContactPhone(phoneNum);
                    message.setDate(getDate());
                    message.setStatus(false);
                    messages.add(message);
                    notification.setProgress(progressMax, ++progress, false);
                    notificationManager.notify(2, notification.build());
                    SystemClock.sleep(2000);
                }
                notification.setContentText("sending finished")
                        .setProgress(0, 0, false)
                        .setOngoing(false);
                notificationManager.notify(2, notification.build());
                broadCastReceiver();
            }
        }).start();
    }
    private void saveMessage(Message messages){

        messageViewModel.insertMessage(messages);
    }

    private void CreateRequest(String id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://docs.google.com/spreadsheets/d/")
                .addConverterFactory(new CsvConverterFactory())
                .build();

        contactApi = retrofit.create(ContactApi.class);
        Call<List<Contact>> call = contactApi.getContact(id);
        call.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                mView.getPhoneNumbers(response.body());
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });


    }

    private String getDate() {
        String currentDate = new SimpleDateFormat("EEE, d MMM", Locale.getDefault()).format(new Date())
                + "'" + new SimpleDateFormat("HH:mm a", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    private void broadCastReceiver(){

        context.registerReceiver(new BroadcastReceiver(){
            int index=0;

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:{
                        if(index<phoneNums.size()){
                            messages.get(index).setStatus(true);
                            saveMessage(messages.get(index++));
                        }
                        Toast.makeText(context, "message_sent",
                                Toast.LENGTH_SHORT).show();
                        break; }
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            if(index<phoneNums.size())
                                saveMessage(messages.get(index++));
                            break;
                    }
                    if(index==phoneNums.size()){
                        context.unregisterReceiver(this);
                    }

                }

        }, new IntentFilter(SENT));

    }
}
