package com.eltahawy.sender.MainActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.eltahawy.sender.R;

public class simDialog extends AppCompatDialogFragment {
    private RadioGroup radioGroup;
    private int id;
    private String sim;
    private DialogListener listener;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.sim_dialog,null);
        radioGroup=view.findViewById(R.id.radioGroup);
            builder.setView(view)
                    .setTitle("Select Sim")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            id=radioGroup.getCheckedRadioButtonId();
                            RadioButton radioButton=view.findViewById(id);
                            sim=radioButton.getText().toString().trim();
                            listener.selectSIM(sim);
                                }
                    });
            return builder.create();
        }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener=(DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+"must implement DialogListener");
        }
    }


    public interface DialogListener{
       void selectSIM(String sim);
    }
}
