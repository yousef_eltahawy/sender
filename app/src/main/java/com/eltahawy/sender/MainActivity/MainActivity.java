package com.eltahawy.sender.MainActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.eltahawy.sender.NewMessageActivity.NewMessageActivity;
import com.eltahawy.sender.R;
import com.eltahawy.sender.data.RoomDB.Message;
import com.eltahawy.sender.data.RoomDB.ViewModel;
import com.eltahawy.sender.data.adapters.MessageAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import static com.eltahawy.sender.NewMessageActivity.NewMessageActivity.notificationManager;

public class MainActivity extends AppCompatActivity implements simDialog.DialogListener {
    public static final int SAVE_MESSAGE_REQUEST = 1;
    private FloatingActionButton fab;
    private RecyclerView recyclerView;
    private  MessageAdapter adapter;
    public static ViewModel messageViewModel;
    public final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1;
    public final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE=2;
    public static SubscriptionInfo simInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String [] {Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        }

        recyclerView=findViewById(R.id.message_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new MessageAdapter();
        recyclerView.setAdapter(adapter);
        messageViewModel= new ViewModelProvider(this).get(ViewModel.class);
        messageViewModel.getAllMessages().observe(this, new Observer<List<Message>>() {
            @Override
            public void onChanged(List<Message> messages) {
                adapter.setMessages(messages);
                recyclerView.scrollToPosition(messages.size()-1);
            }
        });
        recyclerView.setAdapter(adapter);

        fab= findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, NewMessageActivity.class);
                startActivityForResult(intent,SAVE_MESSAGE_REQUEST);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dual_item,menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        openDialog();
        return true;
    }
    @Override
    public void selectSIM(String sim){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager localSubscriptionManager = SubscriptionManager.from(this);
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String [] {Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
            else{
            if (localSubscriptionManager.getActiveSubscriptionInfoCount() > 1) {
                List localList = localSubscriptionManager.getActiveSubscriptionInfoList();
                if(sim.equals("SIM1"))
                    simInfo = (SubscriptionInfo) localList.get(0);
                else if(sim.equals("SIM2"))
                    simInfo = (SubscriptionInfo) localList.get(1);
            }
            }
        }
    }
    public  void  openDialog(){
        simDialog dialog=new simDialog();
        dialog.show(getSupportFragmentManager(),"sim Dialog");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        notificationManager.cancelAll();
    }
}