package com.eltahawy.sender.data.RoomDB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eltahawy.sender.data.RoomDB.Message;
import java.util.List;
@Dao
public interface RoomDao {
    @Insert
    void InsertMessage(Message messages);

    @Update
    void UpdateMessage(Message message);

    @Delete
    void DeleteMessage(Message message);

    @Query("SELECT * FROM Message_table")
    LiveData<List<Message>> getAllMessages();

    @Insert
    void InsertSheet(Sheet sheet);

    @Update
    void UpdateSheet(Sheet sheet);

    @Delete
    void DeleteSheet(Sheet sheet);

    @Query("SELECT * FROM Sheet_table")
    LiveData<List<Sheet>> getAllSheet();

}
