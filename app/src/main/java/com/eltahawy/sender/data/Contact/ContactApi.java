package com.eltahawy.sender.data.Contact;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
public interface ContactApi {
    @GET("{id}/export?gid=0&format=csv")
    Call<List<Contact>>getContact(@Path("id") String id);
}
