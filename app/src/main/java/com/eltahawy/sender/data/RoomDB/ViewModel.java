package com.eltahawy.sender.data.RoomDB;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ViewModel extends AndroidViewModel {
    private Repository repository;
    private LiveData<List<Message>> allMessages;
    private LiveData<List<Sheet>> allSheets;
    public ViewModel(@NonNull Application application){
        super(application);
        repository=new Repository(application);
        allMessages=repository.getAllMessages();
        allSheets=repository.getAllSheet();
    }
    public void insertMessage(Message messages) {
        repository.insertMessage(messages);
    }
    public void updateMessage(Message message){repository.updateMessage(message);}
    public void deleteMessage(Message message){repository.DeleteMessage(message);}
    public void insertSheet(Sheet sheet) {
        repository.insertSheet(sheet);
    }
    public void updateSheet(Sheet sheet){repository.updateSheet(sheet);}
    public void deleteSheet(Sheet sheet){repository.DeleteSheet(sheet);}

    public LiveData<List<Message>> getAllMessages() {
        return allMessages;
    }
    public LiveData<List<Sheet>> getAllSheets() {
        return allSheets;
    }
}
