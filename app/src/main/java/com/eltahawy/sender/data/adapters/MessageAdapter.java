package com.eltahawy.sender.data.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eltahawy.sender.R;
import com.eltahawy.sender.data.RoomDB.Message;

import java.util.ArrayList;
import java.util.List;
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    private List<Message> Messages = new ArrayList<>();
    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);
        return new MessageHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        Message currentMessage = Messages.get(position);
        holder.phoneNumberTextView.setText(currentMessage.getContactPhone());
        holder.contactNameTextView.setText(currentMessage.getContactName());
        holder.contentTextView.setText("Sms Content: "+currentMessage.getMessageContent());
        holder.sendTimeTextView.setText(currentMessage.getDate());
        if(currentMessage.getStatus()==true)
            holder.statusTextView.setText("Sent");
        else
            holder.statusTextView.setText("Not Sent");
    }
    @Override
    public int getItemCount() {
        return Messages.size();
    }

    public void setMessages(List<Message> Messages) {
        this.Messages = Messages;
        notifyDataSetChanged();
    }
    class MessageHolder extends RecyclerView.ViewHolder {
        private TextView statusTextView;
        private TextView contentTextView;
        private TextView sendTimeTextView;
        private TextView contactNameTextView;
        private TextView phoneNumberTextView;
        public MessageHolder(View itemView) {
            super(itemView);
            statusTextView = itemView.findViewById(R.id.messages_status);
            contentTextView = itemView.findViewById(R.id.ContentTextView);
            sendTimeTextView = itemView.findViewById(R.id.sentTimeTextView);
            contactNameTextView=itemView.findViewById(R.id.ContactNameTextView);
            phoneNumberTextView=itemView.findViewById(R.id.phoneNumberTextView);
        }
    }
}
