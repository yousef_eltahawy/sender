package com.eltahawy.sender.data.RoomDB;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class Repository {
    private RoomDao roomDao;
    private LiveData<List<Message>> allMessages;
    private LiveData<List<Sheet>> allSheet;
    public Repository(Application application){
        RoomDB roomDB= RoomDB.getInstance(application) ;
        roomDao=roomDB.roomDao();
        allMessages=roomDao.getAllMessages();
        allSheet=roomDao.getAllSheet();
    }
    public void insertMessage(Message messages) {
        new InsertMessageAsyncTask(roomDao).execute(messages);
    }
    public void insertSheet(Sheet sheet){
        new InsertSheetAsyncTask(roomDao).execute(sheet);
    }
    public void updateMessage(Message message){ new UpdateMessageAsyncTask(roomDao).execute(message); }
    public void updateSheet(Sheet sheet){
        new UpdateSheetAsyncTask(roomDao).execute(sheet);
    }
    public void DeleteMessage(Message message){new DeleteMessageAsyncTask(roomDao).execute(message);}
    public void DeleteSheet(Sheet sheet){new DeleteSheetAsyncTask(roomDao).execute(sheet);}

    public LiveData<List<Message>> getAllMessages() {
            return allMessages;
        }
    public LiveData<List<Sheet>> getAllSheet() {
        return allSheet;
    }

    private static class InsertMessageAsyncTask extends AsyncTask<Message, Void, Void> {
        private RoomDao roomDao;
        private InsertMessageAsyncTask(RoomDao roomDao) {
            this.roomDao = roomDao;
        }
        @Override
        protected Void doInBackground(Message... messages) {
            roomDao.InsertMessage(messages[0]);
            return null;
        }
    }
    private static class InsertSheetAsyncTask extends AsyncTask<Sheet, Void, Void> {
        private RoomDao roomDao;
        private InsertSheetAsyncTask(RoomDao roomDao) {
            this.roomDao = roomDao;
        }
        @Override
        protected Void doInBackground(Sheet... sheets) {
            roomDao.InsertSheet(sheets[0]);
            return null;
        }
    }
    private static class UpdateMessageAsyncTask extends AsyncTask<Message, Void, Void> {
        private RoomDao roomDao;
        private UpdateMessageAsyncTask(RoomDao roomDao) {
            this.roomDao = roomDao;
        }
        @Override
        protected Void doInBackground(Message... messages) {
            roomDao.UpdateMessage(messages[0]);
            return null;
        }
    }
    private static class UpdateSheetAsyncTask extends AsyncTask<Sheet, Void, Void> {
        private RoomDao roomDao;
        private UpdateSheetAsyncTask(RoomDao roomDao) {
            this.roomDao = roomDao;
        }
        @Override
        protected Void doInBackground(Sheet... sheets) {
            roomDao.UpdateSheet(sheets[0]);
            return null;
        }
    }
    private static class DeleteMessageAsyncTask extends AsyncTask<Message,Void,Void>{
        private RoomDao roomDao;
        private DeleteMessageAsyncTask(RoomDao roomDao){this.roomDao=roomDao;}

        @Override
        protected Void doInBackground(Message... messages) {
            roomDao.DeleteMessage(messages[0]);
            return null;
        }
    }
    private static class DeleteSheetAsyncTask extends AsyncTask<Sheet,Void,Void>{
        private RoomDao roomDao;
        private DeleteSheetAsyncTask(RoomDao roomDao){this.roomDao=roomDao;}

        @Override
        protected Void doInBackground(Sheet... sheets) {
            roomDao.DeleteSheet(sheets[0]);
            return null;
        }
    }
}
