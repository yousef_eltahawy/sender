package com.eltahawy.sender.data.RoomDB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Sheet_table")
public class Sheet {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String sheetName;
    private String sheetId;
    public Sheet(){}

    public int getId() {
        return id;
    }

    public String getSheetName() {
        return sheetName;
    }

    public String getSheetId() {
        return sheetId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public void setSheetId(String sheetId) {
        this.sheetId = sheetId;
    }
}
