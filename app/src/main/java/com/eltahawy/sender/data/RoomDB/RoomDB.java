package com.eltahawy.sender.data.RoomDB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities ={Message.class,Sheet.class},version = 1)
public abstract class RoomDB extends RoomDatabase {
    private static RoomDB Instance;
    public abstract RoomDao roomDao();

    public static synchronized RoomDB getInstance(Context context){
        if(Instance==null){
            Instance= Room.databaseBuilder(context.getApplicationContext(), RoomDB.class,"Room_DB")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return Instance;
    }
}