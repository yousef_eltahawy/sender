package com.eltahawy.sender.data.Contact;

public class Contact {
    private String Name;
    private String Phone;
    private String Message;
    private String Status;
    private String Greeting;
    private String Offer_Code;
    private String Page_Link;
    private String Spare;
    public Contact(){
    }

    public String getPhoneNum() {
        return Phone;
    }

    public String getStatus() {
        return Status;
    }

    public void setPhoneNum(String phoneNum) {
        this.Phone = phoneNum;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getName() {
        return Name;
    }

    public String getMessage() {
        return Message;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public void setGreeting(String greeting) {
        Greeting = greeting;
    }

    public void setOffer_Code(String offer_Code) {
        Offer_Code = offer_Code;
    }

    public void setPage_Link(String page_Link) {
        Page_Link = page_Link;
    }

    public void setSpare(String spare) {
        Spare = spare;
    }

    public String getGreeting() {
        return Greeting;
    }

    public String getOffer_Code() {
        return Offer_Code;
    }

    public String getPage_Link() {
        return Page_Link;
    }

    public String getSpare() {
        return Spare;
    }
}
